Social media presence is vital to your online campaign. 

Any website can be benefitted from a formidable social media account.  All of these elements need to be presented from your website too. You can have social share buttons for Facebook or Twitter. 

But, same cannot be your modus operandi, when it comes to other flashy networks. Some of its colors have to be shown magnificently. One such social media network is Instagram.Instagram translates to absolute flair. You should flaunt your Instagram presence on your website. 

Without doubt, WordPress blogs do get enriched visually. What’s better place to present your feed other than the footer? You ideally want your visitors to see something colorful rather than just the copyright and contact information.Some of the premium themes allow you to display Instagram posts on footer. Unfortunately, not all themes give you that option. Nevertheless, we can always get there manually. 

There are plenty of ways to do that. In this article, we will discuss a couple of easy methods to embed Instagram feed on WordPress blog footer.

Source - https://www.optiux.com/